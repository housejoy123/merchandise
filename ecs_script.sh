#!/bin/bash
SERVICE_NAME="merchandise-prod"
TASK_FAMILY="merchandise-prod"
WORK_SPACE="/var/lib/jenkins/workspace/docker_prod_env/merchandise_prod"

# Create a new task definition for this build
pwd
aws ecs register-task-definition --region ap-south-1  --family ${TASK_FAMILY} --cli-input-json file://${WORK_SPACE}/${TASK_FAMILY}.json

# Update the service with the new task definition and desired count
TASK_REVISION=`aws ecs describe-task-definition --region ap-south-1  --task-definition ${TASK_FAMILY} | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//'`
DESIRED_COUNT=`aws ecs describe-services  --region ap-south-1  --cluster Production --services ${SERVICE_NAME} | egrep "desiredCount" | tr "/" " " | awk '{print $2}' | sed 's/,$//' | head -n 1`
echo 'desired_count: '${DESIRED_COUNT}
if [ ${DESIRED_COUNT} = "0" ]; then
    DESIRED_COUNT="1"
fi

aws ecs update-service  --region ap-south-1  --force-new-deployment  --cluster Production --service ${SERVICE_NAME} --task-definition ${TASK_FAMILY}:${TASK_REVISION} --desired-count ${DESIRED_COUNT}
