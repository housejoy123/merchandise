FROM openjdk:8-jre-alpine

#Timezone
RUN apk add --no-cache tzdata
ENV TZ Asia/Calcutta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ADD ./target/merchandise-prod.jar .
ADD ./elastic-apm-agent-1.8.0.jar .
EXPOSE 9898
#CMD ["java", "-jar", "merchandise-prod.jar"]
CMD ["java", "-javaagent:/elastic-apm-agent-1.8.0.jar", "-Delastic.apm.service_name=reviews", "-Delastic.apm.application_packages=org.housejoy", "-Delastic.apm.server_urls=http://elkshire.housejoy.in:8200", "-jar", "merchandise-prod.jar"]