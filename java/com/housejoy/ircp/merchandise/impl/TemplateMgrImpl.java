package com.housejoy.ircp.merchandise.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.housejoy.ircp.merchandise.bean.MerchandiseBean;
import com.housejoy.ircp.merchandise.bean.PackageBean;
import com.housejoy.ircp.merchandise.bean.ProjectMrzTempBean;
import com.housejoy.ircp.merchandise.bean.ResponseBean;
import com.housejoy.ircp.merchandise.bean.TemplateBean;
import com.housejoy.ircp.merchandise.config.ApplicationConstants;
import com.housejoy.ircp.merchandise.elk.PackageMrzElk;
import com.housejoy.ircp.merchandise.elk.ProjectMrzElk;
import com.housejoy.ircp.merchandise.elk.TemplateElk;
import com.housejoy.ircp.merchandise.helper.MerchandiseHelper;
import com.housejoy.ircp.merchandise.mgr.TemplateMgr;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class TemplateMgrImpl implements TemplateMgr {

	@Autowired
	private TemplateElk templateElk;

	@Autowired
	private ProjectMrzElk projectMrzElk;

	@Autowired
	private PackageMrzElk packageMrzElk;

	@Autowired
	private MerchandiseHelper merchandiseHelper;

	@Override
	public TemplateBean fetchHomeScreen(TemplateBean templateBeanReq) {
		List<TemplateBean> templateBeanList = templateElk.fetchTemplateInfo(templateBeanReq);
//		ResponseBean responseBean = null;
		if (templateBeanList != null && !templateBeanList.isEmpty()) {
			templateBeanList.get(0).setValid(true);
			return templateBeanList.get(0);

		} else {
			templateBeanList.get(0).setValid(false);
			templateBeanList.get(0);
		}
		TemplateBean templateBean = TemplateBean.builder().valid(false).msg("no data found").build();
		return templateBean;
	}

	@Override
	public ResponseBean saveHomeScreen(TemplateBean templateBean) {
		List<TemplateBean> exitingTemplateBeanList = templateElk.fetchTemplateInfo(templateBean);
		if (templateBean.getVersionNumber() == null) {
			UUID uuid = UUID.randomUUID();
			templateBean.setTemplateId(uuid.toString());
			for (TemplateBean exitingTemplateBean : exitingTemplateBeanList) {
				if (exitingTemplateBean.getVersionNumber() != null) {
					templateBean.setVersionNumber(exitingTemplateBean.getVersionNumber() + 1);
				}
			}
			templateBean.setVersionNumber(1);
		} else {
//			templateBean.setTemplateId(templateBean.getTemplateId());
		}

		templateBean = templateElk.saveTemplate(templateBean);
		ResponseBean responseBean = null;
		if (templateBean != null) {

			responseBean = ResponseBean.builder().code(200).isValid(true).templateObject(templateBean)
					.successMsg("success").build();
		} else {

			responseBean = ResponseBean.builder().code(500).isValid(false).errorMsg("error")
					.displayMsg("No Date Found!!").build();
		}
		return responseBean;
	}

	@Override
	public ResponseBean fetchProjectTemplates(Map<String, String> requestMap) {

		requestMap.put("templateName", "PROJECT");
		List<ProjectMrzTempBean> projectMrzTempBeansList = projectMrzElk.fetchProjects(requestMap);
		log.info(projectMrzTempBeansList.toString());
		List<MerchandiseBean> merchandiseProjectList = null;
		ResponseBean responseBean = null;
		if (projectMrzTempBeansList != null && !projectMrzTempBeansList.isEmpty()) {
			merchandiseProjectList = merchandiseHelper.createMerchandiseObject(projectMrzTempBeansList);
		}
		if (merchandiseProjectList != null)
			responseBean = ResponseBean.builder().displayMsg("success").isValid(true).code(200)
					.templateObject(merchandiseProjectList).build();
		else
			responseBean = ResponseBean.builder().displayMsg("success").isValid(true).code(200)
					.templateObject(merchandiseProjectList).build();
		return responseBean;
	}

	@Override
	public ResponseBean fetchPackages(Map<String, String> requestMap) {
		ResponseBean responseBean = null;
		List<PackageBean> packageBeansList = packageMrzElk.fetchPackages(requestMap);
		if (packageBeansList != null && !packageBeansList.isEmpty() && requestMap.containsKey("categoryName")
				&& requestMap.get("categoryName").equalsIgnoreCase("Interior")) {
			System.out.println("fetchPackages--------------->");
			Map<String, List<PackageBean>> map = merchandiseHelper.fetchInteriorPackage(packageBeansList);

			List<PackageBean> packageBeans = new ArrayList<PackageBean>();
			for (Map.Entry<String, List<PackageBean>> entry : map.entrySet()) {
				PackageBean bean = new PackageBean();
				bean.setElevation(entry.getKey());
				bean.setPackageBeansList(entry.getValue());
				packageBeans.add(bean);
			}
			responseBean = ResponseBean.builder().displayMsg("success").isValid(true).code(200)
					.templateObject(packageBeans).build();

			log.info("---------fetchPackages----------->" + map.toString());
			return responseBean;
		}
		if (packageBeansList != null && !packageBeansList.isEmpty()) {

			responseBean = ResponseBean.builder().displayMsg("success").isValid(true).code(200)
					.templateObject(packageBeansList).build();
		} else
			responseBean = ResponseBean.builder().displayMsg("no data found").isValid(false).code(200)
					.templateObject(null).build();
		return responseBean;
	}

	@Override
	public ResponseBean calculatePainting(Map<String, String> requestMap) {
		PackageBean packageBean = merchandiseHelper.calculatePaintCost(requestMap);
		ResponseBean responseBean = null;
		if (packageBean != null)
			responseBean = ResponseBean.builder().code(200).displayMsg("success").isValid(true)
					.templateObject(packageBean).build();
		else
			responseBean = ResponseBean.builder().code(500).displayMsg("paint type not found").isValid(false)
					.templateObject(null).build();
		return responseBean;
	}

}
