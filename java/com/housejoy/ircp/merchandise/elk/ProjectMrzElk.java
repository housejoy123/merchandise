package com.housejoy.ircp.merchandise.elk;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.housejoy.ircp.merchandise.bean.ProjectMrzTempBean;
import com.housejoy.ircp.merchandise.helper.MerchandiseHelper;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class ProjectMrzElk {

	@Autowired
	private RestHighLevelClient restHighLevelClient;

	@Autowired
	private MerchandiseHelper merchandiseHelper;

	public static final String MRZ_PROJECT_TEMPLATE_INDEX = "mrz_templates";
	public static final String MRZ_PROJECT_TEMPLATE_TYPE = "templates";

	public List<ProjectMrzTempBean> fetchProjects(Map<String, String> requestMap) {

		SearchRequest searchRequest = new SearchRequest();
		searchRequest.indices(MRZ_PROJECT_TEMPLATE_INDEX);
		searchRequest.types(MRZ_PROJECT_TEMPLATE_TYPE);

		try {
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			BoolQueryBuilder queryBuilders = QueryBuilders.boolQuery();

			queryBuilders.must(QueryBuilders.termQuery("templateType.keyword", requestMap.get("templateName")));
			if (requestMap.get("city") != null)
				queryBuilders.must(QueryBuilders.termQuery("locations.name.keyword", requestMap.get("city")));
			if (requestMap.get("categoryName") != null)
				queryBuilders.must(QueryBuilders.termQuery("categories.name.keyword", requestMap.get("categoryName")));
			searchSourceBuilder.query(queryBuilders);
			searchRequest.source(searchSourceBuilder);
			log.info("fetchProjects---->" + searchSourceBuilder.toString());
			SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
			return this.convertToObj(searchResponse);
		} catch (Exception e) {
			log.error("fetchProjects", e);
			e.printStackTrace();
		}
		return null;
	}

	private List<ProjectMrzTempBean> convertToObj(SearchResponse searchResponse) {

		List<ProjectMrzTempBean> projectMrzTempBeansList = null;
		log.info("convertToObj----->" + searchResponse.toString());
		try {
			if (searchResponse != null) {
				SearchHit[] searchHit = searchResponse.getHits().getHits();

				projectMrzTempBeansList = new ArrayList<ProjectMrzTempBean>();
				for (SearchHit hit : searchHit) {
					log.info("it's coming------>" + hit.getSourceAsString());
					ProjectMrzTempBean projectMrzTempBean = merchandiseHelper.covertToPMTObj(hit.getSourceAsString());
					if (projectMrzTempBean != null)
						projectMrzTempBeansList.add(projectMrzTempBean);

				}
			}

		} catch (Exception e) {
			log.error(e);
		}
		return projectMrzTempBeansList;
	}

}
