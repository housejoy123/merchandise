package com.housejoy.ircp.merchandise.elk;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.housejoy.ircp.merchandise.bean.TemplateBean;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class TemplateElk {

	@Autowired
	private RestHighLevelClient restHighLevelClient;

	public static final String TEMPLATE_INDEX = "idx_ircp_templates";
	public static final String TEMPLATE_TYPE = "ircp_templates";

	public List<TemplateBean> fetchTemplateInfo(TemplateBean templateBean) {

		try {

			SearchRequest searchRequest = new SearchRequest();
			searchRequest.indices(TEMPLATE_INDEX);
			searchRequest.types(TEMPLATE_TYPE);

			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			BoolQueryBuilder queryBuilders = QueryBuilders.boolQuery();
			if (templateBean.getTemplateName() != null)
				queryBuilders.must(QueryBuilders.termQuery("templateName.keyword", templateBean.getTemplateName()));
			if (templateBean.getVersionNumber() != null)
				queryBuilders.must(QueryBuilders.termQuery("versionNumber", templateBean.getVersionNumber()));
			if (templateBean.getCity() != null)
				queryBuilders.must(QueryBuilders.termQuery("city.keyword", templateBean.getCity()));
			searchSourceBuilder.query(queryBuilders);
			searchRequest.source(searchSourceBuilder);
			log.info("fetchProjects---->" + searchSourceBuilder.toString());

			SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
			return this.getSearchResult(searchResponse);
		} catch (Exception e) {
			log.info("error fetchTemplateInfo:->", e);
			e.printStackTrace();
		}
		return null;
	}

	public TemplateBean saveTemplate(TemplateBean templateBean) {

		try {

			IndexRequest indexRequest = new IndexRequest(TEMPLATE_INDEX, TEMPLATE_TYPE,
					String.valueOf(templateBean.getTemplateId()))
							.source(new ObjectMapper().writeValueAsString(templateBean), XContentType.JSON);

			RefreshRequest request = new RefreshRequest(TEMPLATE_INDEX);

			restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT).forcedRefresh();
			restHighLevelClient.indices().refresh(request, RequestOptions.DEFAULT);
			return templateBean;
		} catch (Exception e) {
			log.info("error fetchTemplateInfo:->", e);
			e.printStackTrace();
		}

		return null;
	}

	private List<TemplateBean> getSearchResult(SearchResponse response) {

		SearchHit[] searchHit = response.getHits().getHits();

		List<TemplateBean> resp = new ArrayList<>();

		for (SearchHit hit : searchHit) {
			resp.add(new ObjectMapper().convertValue(hit.getSourceAsMap(), TemplateBean.class));
		}

		return resp;
	}

}
