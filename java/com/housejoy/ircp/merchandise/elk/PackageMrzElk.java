package com.housejoy.ircp.merchandise.elk;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.housejoy.ircp.merchandise.bean.PackageBean;
import com.housejoy.ircp.merchandise.helper.MerchandiseHelper;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class PackageMrzElk {

	@Autowired
	private RestHighLevelClient restHighLevelClient;

	@Autowired
	private MerchandiseHelper merchandiseHelper;

	public static final String PACKGE_INDEX = "idx_package_details";
	public static final String PACKAGE_TYPE = "package_details";

	public List<PackageBean> fetchPackages(Map<String, String> requestMap) {

		SearchRequest searchRequest = new SearchRequest();
		searchRequest.indices(PACKGE_INDEX);
		searchRequest.types(PACKAGE_TYPE);

		try {
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			BoolQueryBuilder queryBuilders = QueryBuilders.boolQuery();
			if (requestMap.get("city") != null && requestMap.get("categoryName")!=null && !requestMap.get("categoryName").equalsIgnoreCase("Construction"))
				queryBuilders.must(QueryBuilders.termQuery("city.keyword", requestMap.get("city")));
			if (requestMap.get("categoryName") != null && requestMap.get("categoryName")!=null && !requestMap.get("categoryName").equalsIgnoreCase("Construction") )
				queryBuilders.must(QueryBuilders.termQuery("category.keyword", requestMap.get("categoryName")));
			searchSourceBuilder.query(queryBuilders);
			searchRequest.source(searchSourceBuilder);
			searchSourceBuilder.query(queryBuilders);
			searchRequest.source(searchSourceBuilder);
			
			
			log.info("fetchProjects---->" + searchSourceBuilder.toString());

			SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
			return this.convertToObj(searchResponse,requestMap);
		} catch (Exception e) {
			log.error("fetchPackages", e);
			e.printStackTrace();
		}
		return null;
	}

	private List<PackageBean> convertToObj(SearchResponse searchResponse,Map<String, String> requestMap) {

		List<PackageBean> packageBeansList = null;
		log.info("convertToObj----->" + searchResponse.toString());
		try {
			if (searchResponse != null) {
				SearchHit[] searchHit = searchResponse.getHits().getHits();
				packageBeansList = new ArrayList<PackageBean>();
				for (SearchHit hit : searchHit) {
					log.info("it's coming------>" + hit.getSourceAsString());
					packageBeansList.add(merchandiseHelper.convertToPackageObj(hit.getSourceAsString(),requestMap));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return packageBeansList;
	}

}
