package com.housejoy.ircp.merchandise.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.housejoy.ircp.merchandise.bean.FormBean;
import com.housejoy.ircp.merchandise.bean.MerchandiseBean;
import com.housejoy.ircp.merchandise.bean.PackageBean;
import com.housejoy.ircp.merchandise.bean.PackageMetaDataBean;
import com.housejoy.ircp.merchandise.bean.ProjectBean;
import com.housejoy.ircp.merchandise.bean.ProjectMrzTempBean;
import com.housejoy.ircp.merchandise.config.ApplicationConstants;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class MerchandiseHelper {

	public List<MerchandiseBean> createMerchandiseObject(List<ProjectMrzTempBean> projectMrzTempBeansList) {
		List<MerchandiseBean> merchandiseBeansList = new ArrayList<MerchandiseBean>();

		for (ProjectMrzTempBean projectMrzTempBean : projectMrzTempBeansList) {
			MerchandiseBean merchandiseBean = new MerchandiseBean();
			merchandiseBean.setTitle(projectMrzTempBean.getName());
			if (projectMrzTempBean.getProject() != null && projectMrzTempBean.getProject().getElevation() != null)
				merchandiseBean.setDescription(projectMrzTempBean.getProject().getElevation());

			merchandiseBean.setImagesList(projectMrzTempBean.getProject().getImages());
			FormBean displayInfoArea = this.createDisplayObject("Area", projectMrzTempBean.getProject().getArea());
			FormBean displayInfoLocation = this.createDisplayObject("Location",
					projectMrzTempBean.getProject().getLocation());
			List<FormBean> displayInfoList = new ArrayList<FormBean>();
			displayInfoList.add(displayInfoArea);

			displayInfoList.add(displayInfoLocation);
			merchandiseBean.setDisplayInfo(displayInfoList);
			merchandiseBeansList.add(merchandiseBean);
		}

		return merchandiseBeansList;
	}

	public FormBean createDisplayObject(String key, String value) {
		return FormBean.builder().displayName(key).displayValue(value).build();
	}

	public ProjectMrzTempBean covertToPMTObj(String strObject) {
		try {
			ProjectMrzTempBean projectMrzTempBeans = new ProjectMrzTempBean();
			JsonNode jsonNode = new ObjectMapper().readTree(strObject);
			if (jsonNode.has("name")) {
				projectMrzTempBeans.setName(jsonNode.get("name").asText());
			}
			ProjectBean projectBean = new ProjectBean();
			if (jsonNode.get("project").has("area")) {
				projectBean.setArea(jsonNode.get("project").get("area").asText());
			}
			if (jsonNode.get("project").has("elevation")) {
				projectBean.setElevation(jsonNode.get("project").get("elevation").asText());
			}
			if (jsonNode.has("locations")) {
				ArrayNode arrayNode = (ArrayNode) new ObjectMapper().readTree(strObject).get("locations");
				if (arrayNode.isArray()) {
					for (JsonNode jsonNode2 : arrayNode) {
						projectBean.setLocation(jsonNode2.get("name").asText());

					}
				}
			}

			List<String> listOfImages = new ArrayList<String>();
			if (jsonNode.get("project").has("images")) {
				System.out.println(jsonNode.get("project").get("images"));
				ArrayNode arrayNode = (ArrayNode) jsonNode.get("project").get("images");
				if (arrayNode.isArray()) {
					for (JsonNode jsonNode2 : arrayNode) {
						listOfImages.add(jsonNode2.get("imageUrl").asText());

					}
				}
			}
			projectBean.setImages(listOfImages);
			projectMrzTempBeans.setProject(projectBean);
			log.info("projectMrzTempBeans" + projectMrzTempBeans.toString());
			return projectMrzTempBeans;

		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("it's null");
		return null;

	}

	public PackageBean convertToPackageObj(String strObj, Map<String, String> requestMap) {
		PackageBean packageBean = null;
		try {

			JsonNode jsonNode = new ObjectMapper().readTree(strObj);
			packageBean = new PackageBean();
			packageBean.setName(jsonNode.get("title").asText());
			if (jsonNode.get("elevation") != null)
				packageBean.setElevation(jsonNode.get("elevation").asText());
			packageBean.setCategoty(jsonNode.get("category").asText());
			packageBean.setCostCalculated("@₹" + jsonNode.get("subTitle").asText());
			if (packageBean.getCostCalculated() != null && (requestMap.get("categoryName") == null
					|| requestMap.get("categoryName").equalsIgnoreCase("Construction"))) {
				String[] cost = packageBean.getCostCalculated().split("/");
				String costSqft[] = cost[0].split("-");
				packageBean = this.calculteCost(Integer.parseInt(costSqft[1]), requestMap, packageBean);
			}
			if (packageBean.getCostCalculated() != null && (requestMap.get("categoryName") != null
					&& requestMap.get("categoryName").equalsIgnoreCase("Renovation"))) {
				String[] cost = packageBean.getCostCalculated().split("/");
				String costSqft[] = cost[0].split("-");
				packageBean = this.calculteRenovationCost(Integer.parseInt(costSqft[1]), requestMap, packageBean);
			}
			packageBean.setArea(requestMap.get("area"));
			List<PackageMetaDataBean> lisMetaDataBeans = new ArrayList<PackageMetaDataBean>();
			List<PackageMetaDataBean> notesMetaDataBeans = new ArrayList<PackageMetaDataBean>();
			if (jsonNode.has("inclusions")) {
				ArrayNode arrayNode = (ArrayNode) new ObjectMapper().readTree(strObj).get("inclusions");
				if (arrayNode.isArray()) {
					for (JsonNode jsonNode2 : arrayNode) {
						PackageMetaDataBean packageMetaDataBean = new PackageMetaDataBean();
						packageMetaDataBean.setName(jsonNode2.get("name").asText());
						if (jsonNode2.get("iconUrl") != null)
							packageMetaDataBean.setIconUrl(jsonNode2.get("iconUrl").asText());
						ArrayNode arrayNodePoints = (ArrayNode) jsonNode2.get("points");
						List<String> arrayNodeList = Arrays
								.asList(new ObjectMapper().readValue(arrayNodePoints.toString(), String[].class));
						packageMetaDataBean.setPonits(arrayNodeList);
						lisMetaDataBeans.add(packageMetaDataBean);
					}
				}
				packageBean.setListOfPoints(lisMetaDataBeans);
			}
			if (jsonNode.has("notes")) {
				ArrayNode arrayNode = (ArrayNode) new ObjectMapper().readTree(strObj).get("notes");
				if (arrayNode.isArray()) {
					for (JsonNode jsonNode2 : arrayNode) {
						PackageMetaDataBean packageMetaDataBean = new PackageMetaDataBean();
						packageMetaDataBean.setName(jsonNode2.get("name").asText());
						packageMetaDataBean.setIconUrl(jsonNode2.get("iconUrl").asText());
//						ArrayNode arrayNodePoints = (ArrayNode) new ObjectMapper().readTree(jsonNode2.asText()).get("points");
						ArrayNode arrayNodePoints = (ArrayNode) jsonNode2.get("points");
						List<String> arrayNodeList = Arrays
								.asList(new ObjectMapper().readValue(arrayNodePoints.toString(), String[].class));
						packageMetaDataBean.setPonits(arrayNodeList);
						notesMetaDataBeans.add(packageMetaDataBean);
					}
				}
				packageBean.setNotesList(notesMetaDataBeans);
			}

			return packageBean;

		} catch (Exception e) {
			log.error("convertTOPackageObj", e);
			e.printStackTrace();
		}
		return packageBean;
	}

	public Map<String, List<PackageBean>> fetchInteriorPackage(List<PackageBean> listPackageBeans) {
		Map<String, List<PackageBean>> mapList = new HashMap<String, List<PackageBean>>();

		for (PackageBean packageBean : listPackageBeans) {
			if (packageBean.getElevation() != null) {
				if (!mapList.containsKey(packageBean.getElevation())) {
					List<PackageBean> packageBeansList = new ArrayList<PackageBean>();
					packageBeansList.add(packageBean);
					mapList.put(packageBean.getElevation(), packageBeansList);
				} else {
					List<PackageBean> packageBeansList = mapList.get(packageBean.getElevation());
					packageBeansList.add(packageBean);
					mapList.put(packageBean.getElevation(), packageBeansList);
				}
			}
		}
		System.out.println("Map Size:-" + mapList.size());
		return mapList;
	}

	private PackageBean calculteCost(Integer perSqFt, Map<String, String> requestMap, PackageBean packageBean) {
		Integer totalCost = 0;
		Integer groundFloor = 0;
		Integer floors = 0;
		if (requestMap.containsKey("area") && requestMap.containsKey("floors") && requestMap.containsKey("parking")) {
			floors = Integer.parseInt(requestMap.get("floors"));
			groundFloor = Integer.parseInt(requestMap.get("area"))
					- (Integer.parseInt(requestMap.get("area")) * (ApplicationConstants.GROUND_FLOOR / 100))
					- ApplicationConstants.CAR_PARKING;
		} else if (requestMap.containsKey("area") && requestMap.containsKey("floors")
				&& !requestMap.containsKey("parking")) {
			floors = Integer.parseInt(requestMap.get("floors"));
			groundFloor = Integer.parseInt(requestMap.get("area"))
					- (Integer.parseInt(requestMap.get("area")) * (ApplicationConstants.GROUND_FLOOR / 100));
		}

		totalCost = totalCost + (groundFloor * perSqFt);
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 1; i <= floors; i++) {
			Integer floor = Integer.parseInt(requestMap.get("area"))
					- (Integer.parseInt(requestMap.get("area")) * (ApplicationConstants.OTHER_FLOORS / 100));
			map.put(i, floor);
			totalCost = totalCost + (floor * perSqFt);
		}
		packageBean.setMapFloors(map);

		packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(totalCost));

		return packageBean;
	}

	private PackageBean calculteRenovationCost(Integer perSqFt, Map<String, String> requestMap,
			PackageBean packageBean) {

		Integer floors = Integer.parseInt(requestMap.get("floors"));
		Integer area = Integer.parseInt(requestMap.get("area"));
		packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(perSqFt * area * floors));

		return packageBean;
	}
	
	public PackageBean calculatePaintCost(Map<String, String> requestMap) {
		PackageBean packageBean=new PackageBean();
		if(requestMap.containsKey("rePaint") && requestMap.get("rePaint")!=null  && Boolean.parseBoolean(requestMap.get("rePaint")) && Boolean.parseBoolean(requestMap.get("interior"))) {	
			switch (requestMap.get("name")) {
			case ApplicationConstants.PREMIUM_EMULSION_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_PREMIUM_EMULSION));
				break;
			case ApplicationConstants.ROYAL_ASPIRA_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_ROYAL_ASPIRA));
				break;
			case ApplicationConstants.ROYAL_ATMOS_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_ROYAL_ATMOS));
				break;
			case ApplicationConstants.ROYAL_LUXAR_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_ROYAL_LUXARY));
				break;
			case ApplicationConstants.ROYAL_MATT_NAME:	
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_ROYAL_MATT));
				break;
			case ApplicationConstants.TRACTOR_EMULSION_NAME:	
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_TRACTOR_EMULSION));
				break;
			default:
				break;
			}
			packageBean.setArea(requestMap.get("area"));
		}
		if(requestMap.containsKey("rePaint") && requestMap.get("rePaint")!=null  && !Boolean.parseBoolean(requestMap.get("rePaint")) && Boolean.parseBoolean(requestMap.get("interior"))) {	
			switch (requestMap.get("name")) {
			case ApplicationConstants.PREMIUM_EMULSION_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_ACE_EMULSION));
				break;
			case ApplicationConstants.ROYAL_ASPIRA_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_ROYAL_ASPIRA));
				break;
			case ApplicationConstants.ROYAL_ATMOS_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_ROYAL_ATMOS));
				break;
			case ApplicationConstants.ROYAL_LUXAR_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_ROYAL_LUXARY));
				break;
			case ApplicationConstants.ROYAL_MATT_NAME:	
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_ROYAL_MATT));
				break;
			case ApplicationConstants.TRACTOR_EMULSION_NAME:	
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_TRACTOR_EMULSION));
				break;
			default:
				break;
			}
			packageBean.setArea(requestMap.get("area"));
		}
		if(requestMap.containsKey("rePaint") && requestMap.get("rePaint")!=null  && Boolean.parseBoolean(requestMap.get("rePaint")) && !Boolean.parseBoolean(requestMap.get("interior"))) {	
			switch (requestMap.get("name")) {
			case ApplicationConstants.PREMIUM_EMULSION_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.EX_PREMIUM_EMULSION));
				break;
			case ApplicationConstants.ROYAL_ASPIRA_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.EX_ROYAL_ASPIRA));
				break;
			case ApplicationConstants.ROYAL_ATMOS_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.EX_ROYAL_ATMOS));
				break;
			case ApplicationConstants.ROYAL_LUXAR_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.EX_ROYAL_LUXARY));
				break;
			case ApplicationConstants.ROYAL_MATT_NAME:	
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.EX_ROYAL_MATT));
				break;
			case ApplicationConstants.TRACTOR_EMULSION_NAME:	
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.EX_TRACTOR_EMULSION));
				break;
			default:
				break;
			}
			packageBean.setArea(requestMap.get("area"));
		}
		if(requestMap.containsKey("rePaint") && requestMap.get("rePaint")!=null  && !Boolean.parseBoolean(requestMap.get("rePaint")) && !Boolean.parseBoolean(requestMap.get("interior"))) {	
			switch (requestMap.get("name")) {
			case ApplicationConstants.PREMIUM_EMULSION_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.EX_PREMIUM_EMULSION));
				break;
			case ApplicationConstants.ROYAL_ASPIRA_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.EX_ROYAL_ASPIRA));
				break;
			case ApplicationConstants.ROYAL_ATMOS_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.EX_ROYAL_ATMOS));
				break;
			case ApplicationConstants.ROYAL_LUXAR_NAME:
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.EX_ROYAL_LUXARY));
				break;
			case ApplicationConstants.ROYAL_MATT_NAME:	
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_ROYAL_MATT));
				break;
			case ApplicationConstants.TRACTOR_EMULSION_NAME:	
				packageBean.setTotalCost("@₹" + new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area"))* ApplicationConstants.REPAINT_TRACTOR_EMULSION));
				break;
			default:
				break;
			}
			packageBean.setArea(requestMap.get("area"));
		}
		
		
		return packageBean;
	}
}
