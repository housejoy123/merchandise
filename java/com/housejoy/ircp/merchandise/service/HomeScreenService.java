package com.housejoy.ircp.merchandise.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.housejoy.ircp.merchandise.bean.MerchandiseBean;
import com.housejoy.ircp.merchandise.bean.ResponseBean;
import com.housejoy.ircp.merchandise.bean.TemplateBean;
import com.housejoy.ircp.merchandise.mgr.TemplateMgr;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value = "/v1")
@Log4j2
public class HomeScreenService {

	@Autowired
	private TemplateMgr templateMgr;

	@PostMapping(value = "/service/latlong", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public Object fetchScreenInfo(@RequestParam(required = true, name = "vNumber") Integer versionNumber,
			@RequestParam(required = false, name = "deviceType") String deviceType,
			@RequestParam(required = false, name = "lat") String latitude,
			@RequestParam(required = false, name = "long") String longitude,
			@RequestParam(required = false, name = "city") String city,
			@RequestParam(required = false, name = "template") String templateName) {

		TemplateBean templateBean = TemplateBean.builder().build();
		if (templateName != null)
			templateBean.setTemplateName(templateName);
		if (city != null)
			templateBean.setCity(city);
		if (versionNumber != null)
			templateBean.setVersionNumber(versionNumber);
		return templateMgr.fetchHomeScreen(templateBean);
	}

	@PostMapping(value = "/update/home", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Object saveMockScreenInfo(@RequestBody TemplateBean templateServiceBean) {

		return templateMgr.saveHomeScreen(templateServiceBean);

	}

	@PostMapping(value = "/update/merchandise", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Object updateMockScreenInfo(@RequestBody List<MerchandiseBean> merchandiseBeans) {

		log.info("----------->" + merchandiseBeans.toString());
		return merchandiseBeans;
	}

	@PostMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public Object fetchProjects(@RequestParam(required = false, name = "city") String city,
			@RequestParam(required = false, name = "categoryName") String categoryName) {

		Map<String, String> requestParam = new HashMap<String, String>();
		if (city != null && !city.isEmpty())
			requestParam.put("city", city);
		if (categoryName != null && !categoryName.isEmpty())
			requestParam.put("categoryName", categoryName);
		if (!requestParam.isEmpty())
			return templateMgr.fetchProjectTemplates(requestParam);
		else {
			return ResponseBean.builder().displayMsg("city or categoryName can't null").isValid(true).code(400)
					.templateObject(null).build();
		}
	}

	@PostMapping(value = "/packages", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public Object fetchPackages(@RequestParam(required = false, name = "city") String city,
			@RequestParam(required = false, name = "categoryName") String categoryName,
			@RequestParam(required = false, name = "size") String size,
			@RequestParam(required = false, name = "area") String area,
			@RequestParam(required = false, name = "floors") String floors,
			@RequestParam(required = false, name = "car_parking") String parking) {

		Map<String, String> requestParam = new HashMap<String, String>();
		if (city != null && !city.isEmpty())
			requestParam.put("city", city);
		if (categoryName != null && !categoryName.isEmpty())
			requestParam.put("categoryName", categoryName);
		if (size != null && !size.isEmpty())
			requestParam.put("size", size);
		if (area != null && !area.isEmpty() && Integer.parseInt(area) >= 450)
			requestParam.put("area", area);
		else if(!categoryName.equalsIgnoreCase("Interior") && area != null && !area.isEmpty() && Integer.parseInt(area) >= 450) {
			ResponseBean responseBean = ResponseBean.builder().displayMsg("Area should not less than 450")
					.isValid(false).code(400).templateObject(null).build();
			return responseBean;
		}

		if (floors != null && !floors.isEmpty())
			requestParam.put("floors", floors);
		if (parking != null && !parking.isEmpty())
			requestParam.put("parking", parking);

		return templateMgr.fetchPackages(requestParam);

	}

	@PostMapping(value = "/calculte/painting", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public Object calculatePaiting(@RequestParam(required = false, name = "city") String city,
			@RequestParam(required = true, name = "area") String area,
			@RequestParam(required = true, name = "type") String type,
			@RequestParam(required = true, name = "rePaint") Boolean rePaint,
			@RequestParam(required = true, name = "interior") Boolean interior,
//			@RequestParam(required = true, name = "whereToPaint") String whereToPaint,
			@RequestParam(required = true, name = "name") String name) {

		Map<String, String> requestParam = new HashMap<String, String>();
		if (city != null)
			requestParam.put("city", city);
		if (area != null)
			requestParam.put("area", area);
		if (type != null)
			requestParam.put("type", type);
		if (rePaint != null)
			requestParam.put("rePaint", rePaint.toString());
		if (rePaint != null)
			requestParam.put("interior", interior.toString());
//		if (rePaint != null)
//			requestParam.put("whereToPaint", whereToPaint);
		if (name != null)
			requestParam.put("name", name);

		return templateMgr.calculatePainting(requestParam);

	}

}