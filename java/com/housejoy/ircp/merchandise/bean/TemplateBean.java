package com.housejoy.ircp.merchandise.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Data
@Log4j2
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TemplateBean {

	private String templateId;
	private Integer versionNumber;
	private String cityName;
	private Boolean valid;
	private List<String> cityList;
	private String msg;
	private String city;
	private Integer cityId;
	private MenuBean headerList;
	private MenuBean subComponentHeaderList;
	private String templateName;
	private String category;
	private Boolean form;
	private TemplateBean categoryObject;
	private List<TemplateBean> categoryList;
	private List<TemplateBean> projectsList;
	private List<TemplateBean> others;
	private List<FormBean> formObject;
	private Integer seq;
	private List<MerchandiseBean> components;
	private List<MerchandiseBean> subComponents;
	

	private List<PackageMetaDataBean> subComponentList;

	@Override
	public String toString() {

		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();

			log.error("to String Method", e);
		}
		return jsonString;
	}

}
