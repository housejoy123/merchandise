package com.housejoy.ircp.merchandise.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class RequestBean {
	
	private String imgType;
	private Float vNumber;
	private String resolution;
	private String deviceType;
	private Float latitude;
	private Float longitude;
	private String userType;
	private String platform;
	
}
