package com.housejoy.ircp.merchandise.config;

public class ApplicationConstants {

	public static final Integer GROUND_FLOOR=25;
	public static final Integer OTHER_FLOORS=20;
	public static final Integer CAR_PARKING=135;
	
	public static final Integer REPAINT_TRACTOR_EMULSION=7;
	public static final Integer REPAINT_PREMIUM_EMULSION=9;
	public static final Integer REPAINT_ROYAL_LUXARY=12;
	public static final Integer REPAINT_ROYAL_MATT=17;
	public static final Integer REPAINT_ROYAL_ATMOS=20;
	public static final Integer REPAINT_ROYAL_ASPIRA=22;
	
	public static final Integer REPAINT_ACE_EMULSION=10;
	public static final Integer REPAINT_APEX_ACE_EMULSION=13;
	
	public static final Integer REPAINT_APEX_ULTIMA=16;
	
	public static final Integer REPAINT_APEX_ULTIMA_PROTECH=20;
	
	
	public static final Integer EX_TRACTOR_EMULSION=9;
	public static final Integer EX_PREMIUM_EMULSION=12;
	public static final Integer EX_ROYAL_LUXARY=20;
	public static final Integer EX_ROYAL_MATT=24;
	public static final Integer EX_ROYAL_ATMOS=26;
	public static final Integer EX_ROYAL_ASPIRA=28;
	
	public static final Integer EX_ACE_EMULSION=12;
	public static final Integer EX_APEX_ACE_EMULSION=15;
	
	public static final Integer EX_APEX_ULTIMA=18;
	
	public static final Integer EX_APEX_ULTIMA_PROTECH=21;
	
	public static final String TRACTOR_EMULSION_NAME="TRACTOR_EMULSION";
	public static final String PREMIUM_EMULSION_NAME="PREMIUM_EMULSION";
	public static final String ROYAL_LUXAR_NAME="ROYAL_LUXARY";
	public static final String ROYAL_MATT_NAME="ROYAL_MATT";
	public static final String ROYAL_ATMOS_NAME="ROYAL_ATMOS";
	public static final String ROYAL_ASPIRA_NAME="ROYAL_ASPIRA";
	
	
	
	
	
}


