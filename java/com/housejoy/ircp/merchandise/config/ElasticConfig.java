package com.housejoy.ircp.merchandise.config;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Data
@Configuration
@Log4j2
public class ElasticConfig {
	
	@Value("${elasticsearch.host}")
	private String host;

	@Value("${elasticsearch.port}")
	private int port;

	@Value("${elasticsearch.username}")
	private String userName;

	@Value("${elasticsearch.password}")
	private String password;

	@Bean(destroyMethod = "close")
	public RestHighLevelClient client() {
		if (!StringUtils.isEmpty(userName) && !StringUtils.isEmpty(password)) {
			CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
			credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(userName, password));
			log.info("authenticated elastic host " + host + "  elastic port " + port);
			return new RestHighLevelClient(RestClient.builder(new HttpHost(host, port, "http"))
					.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider)));
		} else {
			log.info("normal elastic host " + host + "  elastic port " + port);
			return new RestHighLevelClient(RestClient.builder(new HttpHost(host, port, "http")));
		}
	}

}
