package com.housejoy.ircp.merchandise.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@Data
public class ApplicationConfig {

	@Value("${client.soa.fetch_city}")
	private String fetchCityUrl;


}
