package com.housejoy.ircp.merchandise.mgr;

import java.util.Map;

import com.housejoy.ircp.merchandise.bean.PackageMetaDataBean;

public interface FaqMgr {

	PackageMetaDataBean saveFaqs(PackageMetaDataBean packageMetaDataBean);

	PackageMetaDataBean fetchFaqs(Map<String, String> requestParam);

}
