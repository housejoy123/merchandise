package com.housejoy.ircp.merchandise.mgr;

import java.util.Map;

import com.housejoy.ircp.merchandise.bean.ResponseBean;
import com.housejoy.ircp.merchandise.bean.TemplateBean;

public interface TemplateMgr {

	TemplateBean fetchHomeScreen(TemplateBean templateBeanReq);

	ResponseBean saveHomeScreen(TemplateBean templateBean);

	ResponseBean fetchProjectTemplates(Map<String, String> requestMap);
	
	ResponseBean fetchPackages(Map<String, String> requestMap);
	
	ResponseBean calculatePainting(Map<String, String> requestMap);

}
