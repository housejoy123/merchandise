package com.housejoy.ircp.merchandise.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.housejoy.ircp.merchandise.bean.FormBean;
import com.housejoy.ircp.merchandise.bean.MerchandiseBean;
import com.housejoy.ircp.merchandise.bean.PackageBean;
import com.housejoy.ircp.merchandise.bean.PackageMetaDataBean;
import com.housejoy.ircp.merchandise.bean.ProjectBean;
import com.housejoy.ircp.merchandise.bean.ProjectMrzTempBean;
import com.housejoy.ircp.merchandise.client.RestClientCall;
import com.housejoy.ircp.merchandise.config.ApplicationConfig;
import com.housejoy.ircp.merchandise.config.ApplicationConstants;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class MerchandiseHelper {

	@Autowired
	private RestClientCall restClient;
	@Autowired
	private ApplicationConfig applicationConfig;

	public List<MerchandiseBean> createMerchandiseObject(List<ProjectMrzTempBean> projectMrzTempBeansList) {
		List<MerchandiseBean> merchandiseBeansList = new ArrayList<MerchandiseBean>();

		for (ProjectMrzTempBean projectMrzTempBean : projectMrzTempBeansList) {
			MerchandiseBean merchandiseBean = new MerchandiseBean();
			merchandiseBean.setTitle(projectMrzTempBean.getName());
			if (projectMrzTempBean.getProject() != null && projectMrzTempBean.getProject().getElevation() != null && !projectMrzTempBean.getProject().getElevation().equalsIgnoreCase("null"))
				merchandiseBean.setDescription(projectMrzTempBean.getProject().getElevation());

			merchandiseBean.setImagesList(projectMrzTempBean.getProject().getImages());
			FormBean displayInfoArea = this.createDisplayObject("Area", projectMrzTempBean.getProject().getArea());
			FormBean displayInfoLocation = this.createDisplayObject("Location",
					projectMrzTempBean.getProject().getLocation());
			List<FormBean> displayInfoList = new ArrayList<FormBean>();
			displayInfoList.add(displayInfoArea);

			displayInfoList.add(displayInfoLocation);
			merchandiseBean.setDisplayInfo(displayInfoList);
			merchandiseBeansList.add(merchandiseBean);
		}

		return merchandiseBeansList;
	}

	public FormBean createDisplayObject(String key, String value) {
		return FormBean.builder().displayName(key).displayValue(value).build();
	}

	public ProjectMrzTempBean covertToPMTObj(String strObject) {
		try {
			ProjectMrzTempBean projectMrzTempBeans = new ProjectMrzTempBean();
			JsonNode jsonNode = new ObjectMapper().readTree(strObject);
			if (jsonNode.has("name")) {
				projectMrzTempBeans.setName(jsonNode.get("name").asText());
			}
			if (jsonNode.has("name")) {
				String projectNameArray[] = jsonNode.get("name").asText().split(" ");
				projectMrzTempBeans.setProjectId(Integer.parseInt(projectNameArray[1].trim()));
			}
			ProjectBean projectBean = new ProjectBean();
			if (jsonNode.get("project").has("area") && jsonNode.get("project").get("area")!=null && !jsonNode.get("project").get("area").asText().equalsIgnoreCase("null")) {
				projectBean.setArea(jsonNode.get("project").get("area").asText());
			}
			if (jsonNode.get("project").has("area") && (jsonNode.get("project").get("area") == null || jsonNode.get("project").get("area").asText().equalsIgnoreCase("null"))
					&& jsonNode.get("project").has("configuration")
					&& jsonNode.get("project").get("configuration") != null) {
				projectBean.setArea(jsonNode.get("project").get("configuration").asText());
				projectBean.setElevation(jsonNode.get("project").get("configuration").asText());
			}
			if (jsonNode.get("project").has("elevation") && jsonNode.get("project").get("elevation")!=null) {
				projectBean.setElevation(jsonNode.get("project").get("elevation").asText());
			}
			if (jsonNode.has("locations")) {
				ArrayNode arrayNode = (ArrayNode) new ObjectMapper().readTree(strObject).get("locations");
				if (arrayNode.isArray()) {
					for (JsonNode jsonNode2 : arrayNode) {
						projectBean.setLocation(jsonNode2.get("name").asText());

					}
				}
			}

			List<String> listOfImages = new ArrayList<String>();
			if (jsonNode.get("project").has("images")) {
				System.out.println(jsonNode.get("project").get("images"));
				ArrayNode arrayNode = (ArrayNode) jsonNode.get("project").get("images");
				if (arrayNode.isArray()) {
					for (JsonNode jsonNode2 : arrayNode) {
						listOfImages.add(jsonNode2.get("imageUrl").asText());

					}
				}
			}
			projectBean.setImages(listOfImages);
			projectMrzTempBeans.setProject(projectBean);
			log.info("projectMrzTempBeans" + projectMrzTempBeans.toString());
			return projectMrzTempBeans;

		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("it's null");
		return null;

	}

	public PackageBean convertToPackageObj(String strObj, Map<String, String> requestMap) {
		PackageBean packageBean = null;
		try {

			JsonNode jsonNode = new ObjectMapper().readTree(strObj);
			packageBean = new PackageBean();
			packageBean.setName(jsonNode.get("title").asText());
			if (jsonNode.get("elevation") != null)
				packageBean.setElevation(jsonNode.get("elevation").asText());
			packageBean.setCategoty(jsonNode.get("category").asText());
			packageBean.setCostCalculated("₹" + jsonNode.get("subTitle").asText());
			if (packageBean.getCostCalculated() != null && requestMap.get("categoryName") != null
					&& requestMap.get("categoryName").equalsIgnoreCase("Construction")) {
				String[] cost = packageBean.getCostCalculated().split("/");
				String costSqft[] = cost[0].split("-");
				packageBean = this.calculteCost(Integer.parseInt(costSqft[1]), requestMap, packageBean);
			}
			if (packageBean.getCostCalculated() != null && (requestMap.get("categoryName") != null
					&& requestMap.get("categoryName").equalsIgnoreCase("Renovation"))) {
				String[] cost = packageBean.getCostCalculated().split("/");
				String costSqft[] = cost[0].split("-");
				packageBean = this.calculteRenovationCost(Integer.parseInt(costSqft[1]), requestMap, packageBean);
			}
			if(packageBean.getArea()==null || packageBean.getArea().isEmpty())
				packageBean.setArea(requestMap.get("area"));
			List<PackageMetaDataBean> lisMetaDataBeans = new ArrayList<PackageMetaDataBean>();
			List<PackageMetaDataBean> notesMetaDataBeans = new ArrayList<PackageMetaDataBean>();
			if (jsonNode.has("inclusions")) {
				ArrayNode arrayNode = (ArrayNode) new ObjectMapper().readTree(strObj).get("inclusions");
				if (arrayNode.isArray()) {
					for (JsonNode jsonNode2 : arrayNode) {
						PackageMetaDataBean packageMetaDataBean = new PackageMetaDataBean();
						packageMetaDataBean.setName(jsonNode2.get("name").asText());
						if (jsonNode2.get("iconUrl") != null) {
							packageMetaDataBean.setIconUrl(jsonNode2.get("iconUrl").asText());
						}
						ArrayNode arrayNodePoints = (ArrayNode) jsonNode2.get("points");
						List<String> arrayNodeList = Arrays
								.asList(new ObjectMapper().readValue(arrayNodePoints.toString(), String[].class));
						packageMetaDataBean.setPonits(arrayNodeList);
						lisMetaDataBeans.add(packageMetaDataBean);
					}
				}
				packageBean.setListOfPoints(lisMetaDataBeans);
			}
			if (jsonNode.has("notes")) {
				ArrayNode arrayNode = (ArrayNode) new ObjectMapper().readTree(strObj).get("notes");
				if (arrayNode.isArray()) {
					for (JsonNode jsonNode2 : arrayNode) {
						PackageMetaDataBean packageMetaDataBean = new PackageMetaDataBean();
						packageMetaDataBean.setName(jsonNode2.get("name").asText());
						if (jsonNode2.has("iconUrl") && jsonNode2.get("iconUrl") != null) {
							packageMetaDataBean.setIconUrl(jsonNode2.get("iconUrl").asText());
						}
//						ArrayNode arrayNodePoints = (ArrayNode) new ObjectMapper().readTree(jsonNode2.asText()).get("points");
						ArrayNode arrayNodePoints = (ArrayNode) jsonNode2.get("points");
						List<String> arrayNodeList = Arrays
								.asList(new ObjectMapper().readValue(arrayNodePoints.toString(), String[].class));
						packageMetaDataBean.setPonits(arrayNodeList);
						notesMetaDataBeans.add(packageMetaDataBean);
					}
				}
				packageBean.setNotesList(notesMetaDataBeans);
			}

			return packageBean;

		} catch (Exception e) {
			log.error("convertTOPackageObj", e);
			e.printStackTrace();
		}
		return packageBean;
	}

	public Map<String, List<PackageBean>> fetchInteriorPackage(List<PackageBean> listPackageBeans) {
		log.info("fetchInteriorPackage----------->" + listPackageBeans.toString());
		Map<String, List<PackageBean>> mapList = new LinkedHashMap<String, List<PackageBean>>();

		for (PackageBean packageBean : listPackageBeans) {
			if (packageBean.getElevation() != null && !mapList.containsKey(packageBean.getElevation())) {

				List<PackageBean> packageBeansList = new ArrayList<PackageBean>();
				packageBeansList.add(packageBean);
				mapList.put(packageBean.getElevation(), packageBeansList);
			} else {
				List<PackageBean> packageBeansList = mapList.get(packageBean.getElevation());
				packageBeansList.add(packageBean);
				mapList.put(packageBean.getElevation(), packageBeansList);
			}

		}
		System.out.println("Map Size:-" + mapList.size());
		return mapList;
	}

	private PackageBean calculteCost(Integer perSqFt, Map<String, String> requestMap, PackageBean packageBean) {
		Double totalCost = 0d;
		Double groundFloor = 0d;
		Integer floors = 0;
		Double totalAreaDouble=0d;

		if (requestMap.containsKey("area") && requestMap.containsKey("floors") && requestMap.containsKey("parking")
				&& Boolean.parseBoolean(requestMap.get("parking"))) {
			floors = Integer.parseInt(requestMap.get("floors"));
			groundFloor = Double.parseDouble(requestMap.get("area"))
					- (Double.parseDouble(requestMap.get("area")) * (ApplicationConstants.GROUND_FLOOR / 100))
					- ApplicationConstants.CAR_PARKING;
			totalAreaDouble=totalAreaDouble+groundFloor;
		} else if (requestMap.containsKey("area") && requestMap.containsKey("floors")
				&& (!requestMap.containsKey("parking") || !Boolean.parseBoolean(requestMap.get("parking")))) {
			floors = Integer.parseInt(requestMap.get("floors"));
			groundFloor = Double.parseDouble(requestMap.get("area"))
					- (Double.parseDouble(requestMap.get("area")) * (ApplicationConstants.GROUND_FLOOR / 100));
			totalAreaDouble=totalAreaDouble+groundFloor;
		}
		totalCost = totalCost + (groundFloor * perSqFt);
		Map<Integer, Double> map = new HashMap<Integer, Double>();
		for (int i = 1; i <= floors; i++) {
			log.info("area:-" + Integer.parseInt(requestMap.get("area")));
			log.info("other floors value:-"
					+ Integer.parseInt(requestMap.get("area")) * (ApplicationConstants.OTHER_FLOORS / 100));
			log.info(Integer.parseInt(requestMap.get("area")) + "* multiply"
					+ (ApplicationConstants.OTHER_FLOORS / 100));
			Double floor = Double.parseDouble(requestMap.get("area"))
					- (Double.parseDouble(requestMap.get("area")) * (ApplicationConstants.OTHER_FLOORS / 100));
			map.put(i, floor);
			totalAreaDouble=totalAreaDouble+floor;
			totalCost = totalCost + (floor * perSqFt);
		}
		packageBean.setMapFloors(map);
		packageBean.setArea(String.valueOf(totalAreaDouble));
		packageBean.setTotalCost("₹" + new DecimalFormat("#,###.00").format(totalCost));

		return packageBean;
	}

	private PackageBean calculteRenovationCost(Integer perSqFt, Map<String, String> requestMap,
			PackageBean packageBean) {

		Integer floors = Integer.parseInt(requestMap.get("floors"));
		Integer area = Integer.parseInt(requestMap.get("area"));
		packageBean.setTotalCost("₹" + new DecimalFormat("#,###.00").format(perSqFt * area * floors));

		return packageBean;
	}

	public PackageBean calculatePaintCost(Map<String, String> requestMap) {
		PackageBean packageBean = new PackageBean();
		Double extraCal = 0d;
		if (requestMap.containsKey("type") && requestMap.get("type").equalsIgnoreCase("Walls"))
			extraCal = 1.3d;
		if (requestMap.containsKey("type") && requestMap.get("type").equalsIgnoreCase("Ceiling"))
			extraCal = 1.0d;
		if (requestMap.containsKey("type") && requestMap.get("type").equalsIgnoreCase("Both"))
			extraCal = 2.3d;
		if (requestMap.containsKey("rePaint") && requestMap.get("rePaint") != null
				&& Boolean.parseBoolean(requestMap.get("rePaint"))
				&& Boolean.parseBoolean(requestMap.get("interior"))) {

			switch (requestMap.get("name")) {

			case ApplicationConstants.TRACTOR_EMULSION_NAME:

				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.REPAINT_TRACTOR_EMULSION,
						requestMap, extraCal);
				break;
			case ApplicationConstants.PREMIUM_EMULSION_NAME:

				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.REPAINT_PREMIUM_EMULSION,
						requestMap, extraCal);
				break;
			case ApplicationConstants.ROYAL_LUXAR_NAME:

				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.REPAINT_ROYAL_LUXARY, requestMap,
						extraCal);
				break;
			case ApplicationConstants.ROYAL_MATT_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.REPAINT_ROYAL_MATT, requestMap,
						extraCal);
				break;
			case ApplicationConstants.ROYAL_ASPIRA_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.REPAINT_ROYAL_ASPIRA, requestMap,
						extraCal);
				break;
			case ApplicationConstants.ROYAL_ATMOS_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.REPAINT_ROYAL_ATMOS, requestMap,
						extraCal);
				break;
			default:
				break;
			}
		}
		if (requestMap.containsKey("rePaint") && requestMap.get("rePaint") != null
				&& !Boolean.parseBoolean(requestMap.get("rePaint"))
				&& Boolean.parseBoolean(requestMap.get("interior"))) {
			switch (requestMap.get("name")) {
			case ApplicationConstants.TRACTOR_EMULSION_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.PAINT_TRACTOR_EMULSION,
						requestMap, extraCal);
				break;
			case ApplicationConstants.PREMIUM_EMULSION_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.PAINT_PREMIUM_EMULSION,
						requestMap, extraCal);
				break;
			case ApplicationConstants.ROYAL_LUXAR_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.PAINT_ROYAL_LUXARY, requestMap,
						extraCal);
				break;
			case ApplicationConstants.ROYAL_MATT_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.PAINT_ROYAL_MATT, requestMap,
						extraCal);
				break;
			case ApplicationConstants.ROYAL_ATMOS_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.PAINT_ROYAL_ATMOS, requestMap,
						extraCal);
				break;
			case ApplicationConstants.ROYAL_ASPIRA_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.PAINT_ROYAL_ASPIRA, requestMap,
						extraCal);
				break;
			default:
				break;
			}
		}
		if (requestMap.containsKey("rePaint") && requestMap.get("rePaint") != null
				&& Boolean.parseBoolean(requestMap.get("rePaint"))
				&& !Boolean.parseBoolean(requestMap.get("interior"))) {
			switch (requestMap.get("name")) {

			case ApplicationConstants.EX_ACE_EMULSION_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.REPAINT_EX_ACE_EMULSION,
						requestMap, extraCal);
				break;
			case ApplicationConstants.EX_APEX_EMULSION_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.REPAINT_EX_APEX_EMULSION,
						requestMap, extraCal);

				break;
			case ApplicationConstants.EX_APEX_ULTIMA_NAME:

				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.REPAINT_EX_APEX_ULTIMA,
						requestMap, extraCal);
				break;
			case ApplicationConstants.EX_APEX_ULTIMA_PROTECH_NAME:

				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.REPAINT_EX_APEX_ULTIMA_PROTECH,
						requestMap, extraCal);
				break;
			default:
				break;
			}
		}
		if (requestMap.containsKey("rePaint") && requestMap.get("rePaint") != null
				&& !Boolean.parseBoolean(requestMap.get("rePaint"))
				&& !Boolean.parseBoolean(requestMap.get("interior"))) {
			switch (requestMap.get("name")) {
			case ApplicationConstants.EX_ACE_EMULSION_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.PAINT_EX_ACE_EMULSION,
						requestMap, extraCal);
				break;
			case ApplicationConstants.EX_APEX_EMULSION_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.PAINT_EX_APEX_EMULSION,
						requestMap, extraCal);
				break;
			case ApplicationConstants.EX_APEX_ULTIMA_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.PAINT_EX_APEX_ULTIMA, requestMap,
						extraCal);
				break;
			case ApplicationConstants.EX_APEX_ULTIMA_PROTECH_NAME:
				packageBean = this.updatePackageBean(packageBean, ApplicationConstants.PAINT_EX_APEX_ULTIMA_PROTECH,
						requestMap, extraCal);
				break;
			default:
				break;
			}

		}
		packageBean.setCGst(ApplicationConstants.CGST);
		packageBean.setSGst(ApplicationConstants.SGST);

		return packageBean;
	}

	private PackageBean updatePackageBean(PackageBean packageBean, Integer rate, Map<String, String> requestMap,
			Double extraCal) {
		System.out.println(((Integer.parseInt(requestMap.get("area")) * extraCal)));
		packageBean.setTotalAmt("₹"
				+ new DecimalFormat("#,###.00").format(((Integer.parseInt(requestMap.get("area")) * extraCal) * rate)
						* (ApplicationConstants.GST_PERCENTAGE / 100)
						+ (Integer.parseInt(requestMap.get("area")) * extraCal) * rate));
		packageBean.setGstAmount("₹"
				+ new DecimalFormat("#,###.00").format(((Integer.parseInt(requestMap.get("area")) * extraCal) * rate)
						* (ApplicationConstants.GST_PERCENTAGE / 100)));
		packageBean.setTotalCost("₹"
				+ new DecimalFormat("#,###.00").format((Integer.parseInt(requestMap.get("area")) * extraCal) * rate));
		packageBean.setCostCalculated("₹ " + rate + "/sqft");
		packageBean.setArea(
				new DecimalFormat("#,###.00").format(Integer.parseInt(requestMap.get("area")) * extraCal) + " sqft");
		return packageBean;
	}

	public String fetchCityName(String latitude, String longitude) {

		try {
			LinkedMultiValueMap<String, String> hashFormParam = new LinkedMultiValueMap<>();
			hashFormParam.add("lat", latitude);
			hashFormParam.add("long", longitude);
			Map<String, String> headersMap = new HashMap<>();
			headersMap.put("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);

			@SuppressWarnings("unchecked")
			ResponseEntity<String> leadCheckLocResponse = (ResponseEntity<String>) restClient
					.call(applicationConfig.getFetchCityUrl(), headersMap, hashFormParam, HttpMethod.POST);

			if (leadCheckLocResponse.getBody() != null) {
				log.info("HubId:-" + leadCheckLocResponse.getBody());

				JsonNode jsonNode = new ObjectMapper().readTree(leadCheckLocResponse.getBody());
				if (jsonNode.has("isValid") && jsonNode.get("isValid").asBoolean()) {
					return jsonNode.get("cityHubMap").get("cityId").asText();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
