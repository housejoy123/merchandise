package com.housejoy.ircp.merchandise.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Data
@Log4j2
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class PackageMetaDataBean {

	private String id;
	private String title;
	private String name;
	private String componentType;
	private String iconUrl;
	private String category;
	private String imageUrl;
	private String description;
	private String targetPage;
	private String target;
	private List<PackageMetaDataBean> listOfPoints;
	private List<PackageMetaDataBean> itemList;
	private List<String> ponits;

	@Override
	public String toString() {

		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();

			log.error("to String Method", e);
		}
		return jsonString;
	}

}
