package com.housejoy.ircp.merchandise.elk;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.housejoy.ircp.merchandise.bean.PackageMetaDataBean;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class FaqMgrElk {

	@Autowired
	private RestHighLevelClient restHighLevelClient;

	public static final String FAQ_INDEX = "idx_irc_faq";
	public static final String FQA_TYPE = "irc_faq";

	public PackageMetaDataBean saveFaq(PackageMetaDataBean faqDataBean) {

		try {

			IndexRequest indexRequest = new IndexRequest(FAQ_INDEX, FQA_TYPE, String.valueOf(faqDataBean.getId()))
					.source(new ObjectMapper().writeValueAsString(faqDataBean), XContentType.JSON);

			RefreshRequest request = new RefreshRequest(FAQ_INDEX);

			restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT).forcedRefresh();
			restHighLevelClient.indices().refresh(request, RequestOptions.DEFAULT);
			return faqDataBean;
		} catch (Exception e) {
			log.info("error fetchTemplateInfo:->", e);
			e.printStackTrace();
		}

		return null;
	}

	public List<PackageMetaDataBean> fetchFaq(Map<String, String> requestMap) {

		SearchRequest searchRequest = new SearchRequest();
		searchRequest.indices(FAQ_INDEX);
		searchRequest.types(FQA_TYPE);

		try {
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			BoolQueryBuilder queryBuilders = QueryBuilders.boolQuery();

//			queryBuilders.must(QueryBuilders.termQuery("templateType.keyword", requestMap.get("templateName")));
			if (requestMap.get("city") != null)
				queryBuilders.must(QueryBuilders.matchPhrasePrefixQuery("locations.name", requestMap.get("city")));
//				queryBuilders.must(QueryBuilders.termQuery("locations.name.keyword", requestMap.get("city")));
			if (requestMap.get("categoryName") != null)
				queryBuilders.must(QueryBuilders.matchPhrasePrefixQuery("category", requestMap.get("categoryName")));
//				queryBuilders.must(QueryBuilders.termQuery("categories.name.keyword", requestMap.get("categoryName")));

			searchSourceBuilder.query(queryBuilders);
			searchSourceBuilder.size(1000);
			searchRequest.source(searchSourceBuilder);
			log.info("fetchProjects---->" + searchSourceBuilder.toString());
			SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
			return this.convertToObj(searchResponse);
		} catch (Exception e) {
			log.error("fetchProjects", e);
			e.printStackTrace();
		}

		return null;
	}

	private List<PackageMetaDataBean> convertToObj(SearchResponse searchResponse) {

		List<PackageMetaDataBean> projectMrzTempBeansList = null;
		log.info("convertToObj----->" + searchResponse.toString());
		try {
			if (searchResponse != null) {
				SearchHit[] searchHit = searchResponse.getHits().getHits();

				projectMrzTempBeansList = new ArrayList<PackageMetaDataBean>();
				for (SearchHit hit : searchHit) {
					log.info("it's coming------>" + hit.getSourceAsString());
					projectMrzTempBeansList.add(new ObjectMapper().convertValue(hit.getSourceAsMap(), PackageMetaDataBean.class));
				}

			}
		} catch (Exception e) {
			log.error(e);
		}
		return projectMrzTempBeansList;
	}

}
