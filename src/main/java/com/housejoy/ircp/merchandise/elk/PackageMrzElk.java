package com.housejoy.ircp.merchandise.elk;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.housejoy.ircp.merchandise.bean.PackageBean;
import com.housejoy.ircp.merchandise.helper.MerchandiseHelper;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class PackageMrzElk {

	@Autowired
	private RestHighLevelClient restHighLevelClient;

	@Autowired
	private MerchandiseHelper merchandiseHelper;

	public static final String PACKGE_INDEX = "idx_package_details_v2";
	public static final String PACKAGE_TYPE = "idx_package_details_v2";
	final LinkedList<String> packageCategoryList = new LinkedList<String>();

	public List<PackageBean> fetchPackages(Map<String, String> requestMap) {

		SearchRequest searchRequest = new SearchRequest();
		searchRequest.indices(PACKGE_INDEX);
		searchRequest.types(PACKAGE_TYPE);

		try {
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			BoolQueryBuilder queryBuilders = QueryBuilders.boolQuery();
			if (requestMap.get("city") != null)
				queryBuilders.must(QueryBuilders.termQuery("city.keyword", requestMap.get("city")));

			if (requestMap.get("categoryName") != null & requestMap.get("categoryName").split(" ").length > 1)
				queryBuilders.must(QueryBuilders.fuzzyQuery("category.keyword", requestMap.get("categoryName").split(" ")[0]));

			if (requestMap.get("categoryName") != null && requestMap.get("categoryName") != null
					&& requestMap.get("categoryName").split(" ").length == 1)
				queryBuilders.must(QueryBuilders.termQuery("category.keyword", requestMap.get("categoryName")));

			if (requestMap.get("cityId") != null)
				queryBuilders.must(QueryBuilders.termQuery("cityId", requestMap.get("cityId")));
			searchSourceBuilder.query(queryBuilders);
			searchRequest.source(searchSourceBuilder);
			searchSourceBuilder.query(queryBuilders);
			searchRequest.source(searchSourceBuilder);
			log.info("fetchPackages--------->" + searchSourceBuilder.toString());
			SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

			List<PackageBean> packageBeansList = this.convertToObj(searchResponse, requestMap);
			List<PackageBean> newPackageBeansList = new LinkedList<PackageBean>();
			newPackageBeansList.addAll(packageBeansList);
//			Collections.sort(newPackageBeansList,
//					Comparator.comparing(item -> packageCategoryList.indexOf(item.getName())));
//			newPackageBeansList.sort(Comparator.comparing(v->packageCategoryList.indexOf(v.getName())));
			return getSortList(newPackageBeansList);

//			return newPackageBeansList;
		} catch (Exception e) {
			log.error("fetchPackages", e);
			e.printStackTrace();
		}
		return null;
	}

	private List<PackageBean> convertToObj(SearchResponse searchResponse, Map<String, String> requestMap) {

		List<PackageBean> packageBeansList = null;
		try {
			if (searchResponse != null) {
				log.info("convertToObj----------->" + searchResponse.toString());
				SearchHit[] searchHit = searchResponse.getHits().getHits();
				packageBeansList = new ArrayList<PackageBean>();
				for (SearchHit hit : searchHit) {
					packageBeansList.add(merchandiseHelper.convertToPackageObj(hit.getSourceAsString(), requestMap));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return packageBeansList;
	}

	public static List<PackageBean> getSortList(List<PackageBean> unsortedList) {

		final LinkedList<String> orderList = new LinkedList<String>();
		orderList.add("Standard");
		orderList.add("Premium");
		orderList.add("Super");
		orderList.add("Super Premium");
		orderList.add("Luxury");
		if (unsortedList != null && !unsortedList.isEmpty() && orderList != null && !orderList.isEmpty()) {
			List sortedList = new ArrayList<PackageBean>();
			for (String id : orderList) {
				PackageBean found = getPersonIfFound(unsortedList, id); // search for the item on the list by ID
				if (found != null)
					sortedList.add(found); // if found add to sorted list
				unsortedList.remove(found); // remove added item
			}
			sortedList.addAll(unsortedList); // append the reaming items on the unsorted list to new sorted list
			return sortedList;
		} else {
			return null;
		}

	}

	public static PackageBean getPersonIfFound(List<PackageBean> list, String key) {
		for (PackageBean person : list) {
			if (person.getName().equalsIgnoreCase(key)) {
				return person;
			}
		}
		return null;
	}

}
