package com.housejoy.ircp.merchandise.aspects;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health.Builder;
import org.springframework.stereotype.Component;

@Component
public class MerchandiseHealthIndicator extends AbstractHealthIndicator {

	@Override
	protected void doHealthCheck(Builder builder) throws Exception {

		builder.up().withDetail("application", "Alive and Running!!").withDetail("error", "Nothing! I'm good.");
	}

}
