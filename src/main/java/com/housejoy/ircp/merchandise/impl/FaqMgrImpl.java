package com.housejoy.ircp.merchandise.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.housejoy.ircp.merchandise.bean.PackageMetaDataBean;
import com.housejoy.ircp.merchandise.elk.FaqMgrElk;
import com.housejoy.ircp.merchandise.mgr.FaqMgr;

@Component
public class FaqMgrImpl implements FaqMgr {

	@Autowired
	private FaqMgrElk faqMgrElk;

	@Override
	public PackageMetaDataBean saveFaqs(PackageMetaDataBean packageMetaDataBean) {
		if (packageMetaDataBean.getId() == null) {
			UUID uuid = UUID.randomUUID();
			packageMetaDataBean.setId(uuid.toString());
		}
		return faqMgrElk.saveFaq(packageMetaDataBean);
	}

	@Override
	public PackageMetaDataBean fetchFaqs(Map<String, String> requestParam) {
		List<PackageMetaDataBean> packageMetaDataBeansList = faqMgrElk.fetchFaq(requestParam);
		if (packageMetaDataBeansList != null && !packageMetaDataBeansList.isEmpty())
			return packageMetaDataBeansList.get(0);
		return null;
	}

}
